package hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hello.mapper.StudentMapper;
import hello.service.StudentService;


@RestController
public class StudentController {
	
	@Autowired
	private StudentMapper mapper;
	
	@Autowired
	private StudentService service;
	
	@RequestMapping("/")
	public Object hello() {
		System.out.println(service.findName());
		return service.findName();
	}
	
	@RequestMapping("/update")
	public Object update() {
		mapper.updateSexById(1L, "nan");
		
		return mapper.findById(1L);
	}
}
